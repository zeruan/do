Hi! Thanks for using this project. I had a lot of fun building it, and I hope you're having fun using it too.

#### If you have an error or support request

- Read the error message and documentation.
- Search [existing issues](https://gitlab.com/ghostium/doit/issues?assignee_id=&author_id=&label_name=&milestone_title=&scope=all&sort=id_desc&state=closed), and [the internet](https://google.com) first.
- If the issue is with a dependency of this project, post on the dependency's repo.
- If you can fix the issue, submit a Pull Request
- If the issue persists, post on the issue tracker. Include any information that could help others reproduce and fix.

#### If you have a feature proposal or want to contribute

- Post your proposal on the issue tracker so we can review it together. Some proposals may not be a good fit for the project.
- Contribute a Pull Request.
- Respond to code review.
- Watch the Pull Request be merged, and bathe in a job well done

Every piece of software is a work in progress. This project is the result of many hours of work contributed freely by myself and the many people that build the projects it depends on.

Please respect our time, effort, and good will. Issues with a demanding or entitled tone will be closed until reworded.

Modified Source from: https://gist.github.com/robwierzbowski/4252e626e37231797941
