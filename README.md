# Do

## Deprecation Warning

This tool is deprecated. While Do is a nice idea by chaining functions and having a nice command output, which enables to track where a message came from, Makefiles out worth Do. Makefiles were previously used as a task runner, with the requirement that the task runner could be used with almost every programming language. Makefile requires backslashes run multi line commands, which I really hate. But, Makefiles are widely known given Makefiles popularity. Do, on the other hand, requires to users to learn new things and run minified bash code, in which a bad actor could hide malicious actions easily.

Do is a simple build tool which promotes the usage of bash
functionality. Each do task imitates a mini bash script, but bundled in
one file. A do task is a single bash function.

## Setup

### Installation

Copy [do.min.sh](src/do.min.sh) into your project. Do requires GNU core
utilities (coreutils), which are installed on almost every linux system.
Do is tested under a debian + ubuntu bash shell and environment.

### Updating

Copy the first line of [do.min.sh](./src/do.min.sh) into your existing
do file.

## CLI usage

You can do two actions with Do. First is printing out the usage and
available tasks. You can do that by invoking do without an argument.

```bash
./do
```

Example output:

```bash
DO Version 0.1.3
Usage: do <taskname>

Available tasks:
build
build:webpack
build:jsdoc
clean
lint
watch
```

The second action is actually running one task.

```bash
./do <taskname>
```

You can't reference multiple task names that should run or arguments.
That is because it is better to link them together with task
dependencies. If you want pass arguments use environment variables.

## Documentation

### Do file structure

![do minified source code](./doc/img/min-code.png "Minified source code of Do")

The source code of Do is saved in the first line.

After that there is the definition area for bash function/Do tasks.
Define tasks here. Every code after the definition area will never be
reached.

### Define a simple task

To define a Do task just create a bash function in the
[definition area](#do-file-structure).

The bash function name needs to prepended with 'do_'. The name after the
underscore determines the name of the Do task.

Example:

```bash
function do_clean() {
rm -rf build/
}
```

Invoke this task with

```bash
./do clean
```

### Create sub tasks

Defining a sub-task is almost like a normal one. The difference is that
at definition time the task name is split by a _0_ and at
[usage as a dependency](#depend-on-other-tasks) or at cli invocation _:_

Example:

```bash
function do_build0webpack() {
./node_modules/.bin/webpack
}
```

Invoke this task over cli with

```bash
./do build:webpack
```

Invoke as a dependency in a Do task

```bash
dotask "build:webpack"
```

### Depend on other tasks

### Log messages

### API


## Licence

The Licence is found under [LICENCE](LICENCE.md). If you don't like reading
long text, a non-legally binding summary can be found
[here](https://tldrlegal.com/l/mit).
