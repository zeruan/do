#!/usr/bin/env bash

VERSION="0.1.3"

# Pass error codes from commands up to the shell
set -e

# Array where the order of execution chain of the tasks will be saved
DEP_CHAIN=()

# Array where function names will be saved that already run
DONE=()

# Preserve cmd parameters because in function scope $ is associated to function arguments
ARGS=($@)

#Print version and lists available tasks
function _usage() {

     #Print basic infos
     echo -e "DO Version ${VERSION}"
     echo -e "Usage: do <taskname>\n"
     echo -e "Available tasks:"

     # Cat itself, search for functions that start with do_ , split at _ take the second element, replace 0 with : for sub-tasks
     tasks=$(< "${BASH_SOURCE[0]}" grep -Eo "function do_[a-zA-Z0]+" | cut -d "_" -f 2 | tr "0" ":")

     # Don't just print a newline if no task is defined.
     if [ -z "${tasks}" ]
     then
        echo "No tasks are defined"
     else
        echo -e "${tasks}"
     fi
}

# Print error when a task is defined twice and exit
function _checkDuplicates() {
  # Cat itself, search for functions that start with do_ , split at _ take the second element, replace 0 with : for sub-tasks
  tasks=$(< "${BASH_SOURCE[0]}" grep -Eo "function do_[a-zA-Z0]+" | cut -d "_" -f 2 | tr "0" ":")
  tasksasarray=($tasks)
  # Give if tip if a task is redefined (duplicate)
  for task in "${tasksasarray[@]}"
  do
    count=0
    for otherTask in "${tasksasarray[@]}"
    do
       if [ "$task" = "$otherTask" ];
       then
          count=$((count+1))
       fi
    done

    # Warn if task is defined more then once
    if [ "$count" -gt 1 ];
    then
       echo -e "\nError: Task ${task} is defined ${count} times. The last definition overwrites the other ones.\n"
       exit 1
    fi
  done
}

# Handles a task chain
# Called if a task require to run a other task
function __handle_depchain_enter() {

# Add task name to dep chain array
DEP_CHAIN+=($1)
}

# Handle if a task was called by another task, but this task
# finished. It removes the task name from the depchain array.
# It always remove the last element out of the depchain array.
function __handle_depchain_out() {

# Only remove a element if the array isn't empty. (If no sub-task was called)
if [[ ! ${#DEP_CHAIN[@]} -eq 0 ]];
then
  unset DEP_CHAIN[${#DEP_CHAIN[@]}-1]
fi
}

# Checks if a function exists. Returns either true or false.
function __fn_exists() {
    if [[ $(type -t "${1}" 2>/dev/null) == function ]];
    then
      return 1
    else
      return 0
    fi
}

# Main logic of do. Should be called AFTER the tasks have been defined.
# Do not call manually!
function __main() {
# If a task is defined twice fail-fast
_checkDuplicates

# If no task given as argument
if [ ${#ARGS[@]} -lt 1 ]
  then
      # Print usage
      _usage
      exit 1
  else
      # Do task that's given by the cmd argument
      dotask "${ARGS[0]}"
  fi
}

# Semantic function
# Used to mark the start of the task functions definitions.
function _do_task_definition_START() {
:
}

# Semantic and functional function
# Used to mark the end of the task functions definitions.
# Executes main function
function _do_task_definition_END() {
__main
}

# Public function
# The first and only argument is the task name.
# If the function doesn't exist or the argument is empty it will print an error.
# Every task should be only run once per execution of the do script.
# Naming has no underline because than the usage function would
# find it. It can't be called do, as do has syntactic relevance in bash.
function dotask() {

  # Indicate possible incorrect function use to user
  if [ -z "${1}" ]
  then
    log "Error: Passed empty function name"
    exit 1
  fi

  # The resulting function name of the given argument
  # Note: a 0 in a function name in the script equals a : as argument
  local function_name
  function_name="do_""$(echo "${1}" | tr ':' '0')"

  # Check if the user has defined the task
  if __fn_exists "${function_name}"
    then
      log "Error: Task '${1}' has no function!"
      exit 1
    fi

  # Only run a task once
  if ! [[ ${DONE[*]} =~ ${1} ]]
  then
    # Add task name to DONE that it won't run twice
    DONE+=("${1}")

    # Notify user that we entering a task
    log "Executing ${1}"

    # Enter depchain AFTER the notification as the log path
    # should match the environment that it was called it.
    # When a task executes another one, the log function is not invoked
    # inside the sub-task, so the sub-task should'nt be come up at the log
    # function before we call the function
    __handle_depchain_enter "${1}"

    # Execute the task
    ${function_name}

    __handle_depchain_out
  else

    # Notify user that at some point the task already run
    log "Executing ${1} - skipped, it already run."
  fi

}

# Public function for logging
# Takes a string as argument, which is the log message
# It adds the current dependency chain to the log message
function log() {

   # Generate dep chain string
   # Should be in this format: dep/dep/dep/
   local chain
   chain=$(echo "${DEP_CHAIN[*]}" | tr " " "/")

   # If dependency chain is empty add a trailing slash as we want that the default
   # output is DO/>
   if [[ ! ${#DEP_CHAIN[@]} -eq 0 ]]
     then
     chain="${chain}/"
   fi

   # Print log message
   echo -e "DO/${chain}> ${1}"
}

_do_task_definition_START ###############################
# Define tasks here

_do_task_definition_END ###############################
